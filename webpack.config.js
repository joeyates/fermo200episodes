const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const ManifestPlugin = require('webpack-manifest-plugin')
const path = require('path')
const webpack = require('webpack')

const devMode = process.env.NODE_ENV !== 'production'
const miniCss = new MiniCssExtractPlugin({
  filename: devMode ? 'stylesheets/[name].css' : 'stylesheets/[name]-[hash].css',
  chunkFilename: devMode ? 'stylesheets/[id].css' : 'stylesheets/[id]-[hash].css'
})

const mode = process.env.NODE_ENV !== 'development' ? 'production' : 'development'

module.exports = {
  context: __dirname + '/priv/source',
  mode,
  entry: {
    all: './javascripts/all.js',
    application: './stylesheets/application.sass'
  },
  resolve: {
    modules: [
      'javascripts',
      __dirname + '/node_modules'
    ]
  },
  output: {
    path: __dirname + '/build',
    publicPath: '/', // prepend '/' to image paths resolved from 'url()' in SASS
    filename: devMode ? 'javascripts/[name].js' : 'javascripts/[name]-[hash].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {loader: 'import-glob-loader'}
        ]
      },
      {
        test: /\.(gif|jpg|png|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name]-[hash].[ext]', // version image paths
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(eot|ttf|woff|woff2|xml)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]'
            }
          }
        ]
      },
      {
        test: /.*\.sass$/,
        use: [
          {loader: MiniCssExtractPlugin.loader},
          {loader: 'css-loader'},
          {
            loader: 'sass-loader',
            options: {
              includePaths: ['node_modules/normalize-scss/sass']
            }
          },
          {loader: 'import-glob-loader'}
        ]
      }
    ]
  },
  plugins: [
    miniCss,
    new ManifestPlugin()
  ]
}
