defmodule Fermo200Episodes do
  @moduledoc """
  Documentation for Fermo200Episodes.
  """

  use Fermo, %{
    i18n: [:en],
    exclude: [
      "javascripts/*",
      "layouts/*",
      "stylesheets/*",
      "templates/*"
    ]
  }
  use DatoCMS
  use Memoize

  def build do
    config = config()

    Fermo.Assets.build(config)

    {:ok} = DatoCMS.load()

    config = Enum.reduce(episodes(), config, fn (episode, config) ->
      Enum.reduce(1 .. 200, config, fn (n, config) ->
        Fermo.proxy(
          config,
          "templates/episode.html.slim",
          "/episodes/episode-#{episode.id}-#{n}/index.html",
          %{episode: episode},
          %{locale: :en}
        )
      end)
    end)

    Fermo.build(config)
  end

  defmemo season_episodes(%{} = season) do
    by_season = episodes_by_season()
    by_season[season.id]
  end

  defmemo episodes_by_season do
    episodes = episodes()
    # TODO: ensure correct ordering (by `episode_number`)
    Enum.reduce(episodes, %{}, fn (episode, by_season) ->
      {:season, season_id} = episode.season
      season_episodes = by_season[season_id] || []
      put_in(by_season, [season_id], [episode | season_episodes])
    end)
  end

  defmemo episodes do
    episodes = dato_items(:episode, :en)
    Enum.map(episodes, fn episode ->
      if (episode.image != nil) and episode.image.path do
        image_url = dato_image_url(episode.image)
        put_in(episode, [:image_url], image_url)
      else
        episode
      end
    end)
  end

  defmemo season({:season, _season_id} = specifier) do
    dato_item(specifier)
  end
end
