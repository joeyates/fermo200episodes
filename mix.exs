defmodule Fermo200Episodes.MixProject do
  use Mix.Project

  def project do
    [
      app: :fermo200episodes,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:datocms_client, ">= 0.2.4"},
      {:fermo, ">= 0.1.8"},
      {:memoize, ">= 1.3.0"}
    ]
  end
end
