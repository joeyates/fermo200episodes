use Mix.Config

config :datocms_client,
  :api_config,
  %{
    headers: ["Authorization": "Bearer 6d71de8571ec5a8d925c83b4a5087813db3242233d187758b6"],
    options: [timeout: :infinity, recv_timeout: :infinity]
  }

environment_config = "#{Mix.env}.exs"
if File.exists?(Path.join("config", environment_config)) do
  import_config environment_config
end
